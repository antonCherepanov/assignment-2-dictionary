global find_word
global print_dictionary
global print_value
extern string_equals
extern print_string
extern string_length
extern print_newline
;rsi stands for null-terminated string, rdi for dictionary start
;rdi, rsi for comparison in string_length
find_word:
.loop:
	cmp rdi, 0
	je .end
	add rdi, 8
	call string_equals
	cmp rax, 0
	jne .end
	mov rdi, [rdi-8]
	jmp .loop
.end:
	mov rax, rdi
	ret
;rdi has first element`s poiner
print_dictionary:
.loop:
	cmp rdi, 0
	je .end
	add rdi, 8
        push rdi
	call print_value
	pop rdi
	mov rdi, [rdi - 8]
	jmp .loop
.end:
	ret
;rdi has element`s pointer
print_value:
        call string_length
        add rdi, rax
        inc rdi
        call print_string
        call print_newline
	ret
