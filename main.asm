%include "words.inc"

global _start
extern print_string
extern print_newline
extern find_word
extern print_dictionary
extern exit
extern read_key
extern print_value
extern string_equals
section .data
all_key db "all", 0
error_message db "Oh, there is no such reference in the dictionary. Better luck next time!", 0
section .text
_start:
	sub rsp, 255
	mov rdi, rsp
	mov rsi, 255
	call read_key
	mov rsi, rax
	mov rdi, all_key
	call string_equals
	cmp rax, 1
	je .print_all
	mov rdi, lastLabel
	call find_word
	add rsp, 255
	cmp rax, 0
	je .error
	mov rdi, rax
	call print_value
	jmp .end
.error:
	mov rdi, error_message
	call print_string
	call print_newline
	jmp .end
.print_all:
	mov rdi, lastLabel
	call print_dictionary
.end:
	mov rdi, 60
	call exit
