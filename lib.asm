global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global read_key

section .data

newline_char: db 0xA
buffer times 256 db 0
section .lllltext
 
section .text 
; Принимает код возврата и завершает текущий процесс
exit:
    	mov rax, rdi
	xor rdi, rdi
	syscall
	ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    	xor rax, rax
.loop:
	cmp byte [rdi+rax], 0
	je .end
	inc rax
	jmp .loop
.end:
	ret
; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	call string_length
	mov rdx, rax
	mov rax, 1
	mov rsi, rdi
	mov rdi, 1
	syscall
	ret

; Принимает код символа и выводит его в stdout
print_char:
	push rdi
        mov rdx, 1
        mov rax, 1
        mov rsi, rsp
        mov rdi, 1
        syscall
	pop rdi
        ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
     	mov rax, 1 ; 'write' syscall identifier
 	mov rdi, 1 ; stdout file descriptor
 	mov rsi, newline_char
 	mov rdx, 1 ; the amount of bytes to write
 	syscall
 	ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	mov rax, rdi
	mov rdi, 10
	push 0x00
.loop:
	xor rdx, rdx
	div  rdi
	add rdx, '0' ; добавляем ASCII код 0 (смещение)
	push rdx
	test rax, rax  ;аккумулятор обнулится после всех итераций деления
	jnz .loop
.print:
	pop rdi
	cmp rdi, 0
	je .end
 	call print_char
	jmp .print
.end:
	ret
; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	test rdi, rdi
	jns .print
	mov r9, rdi ;сохраняем число
	mov rdi, "-"
	call print_char
	mov rdi, r9
	neg rdi
.print:
	call print_uint
.end:
	ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    	call string_length
	mov rcx, rax
	mov r9, rcx
.loop:
	mov rdx, r9
	sub rdx, rcx
	mov al, byte[rdi+rdx]
	mov dl, byte[rsi+rdx]
	cmp al, dl
	jne .not_equal
	cmp rcx, 0
	je .equal
	dec rcx
	jmp .loop
.not_equal:
	mov rax, 0
	jmp .end
.equal:
	mov rax, 1
	jmp .end
.end:
    	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    	xor rax, rax
	xor rdi, rdi ; stdin
	mov rdx, 1 ; length
	push 0 ; сдвигаем для перезаписи в данную ячейку
	mov rsi, rsp
	syscall
	pop rax
    	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
%macro read 2
%1:
	push rbx
	mov r9, rdi     ; адрес буфера
	mov r8, rsi	; размер буфера.
	xor rbx, rbx
.skip_space:
	call read_char
	test rax, rax
	jz .success
	cmp al, 0x20
	je .skip_space
	cmp al, 0x9
	je .skip_space
	cmp al, 0xA
	je .skip_space
	mov byte[buffer], al
	inc rbx
.read:
	call read_char
%if %2==1
	cmp al, 0x21
	jb .success
%else
	cmp al, 0xA
	je .success
%endif
	mov byte [buffer + rbx], al
	cmp r8, rbx
	jb .failure
	inc rbx
	jmp .read

.success:
	mov rdx, rbx
	mov rax, buffer
	mov byte[buffer+rbx], 0    ; условие
	pop rbx
	ret
.failure:
	xor rax, rax
	pop rbx
	ret
%endmacro
read read_word, 1
read read_key, 0
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	mov rsi, rdi
	xor rdx, rdx
    	xor rax, rax
.parse:
	xor rdi, rdi
	cmp byte [rsi+rdx], '0'
	jb .end
	cmp byte[rsi+rdx], '9'
	ja .end
	mov dil, byte [rsi+rdx] ; запись в rdi
	sub dil, 0x30
	imul rax, 10 ; сдвиг rax для новой цифры
	add rax, rdi
	inc rdx
	jmp .parse
.end:
    	ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    	cmp byte [rdi], '-'
	jne parse_uint
	inc rdi ; пропуск знака
	call parse_uint
	inc rdx
	neg rax
    	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	push rbx
	xor rbx, rbx
	mov rcx, rdx
	call string_length
	cmp rax, rdx
	jg .set_zero
.loop:
	mov bl, byte [rdi+rcx]
        mov byte [rsi+rcx], bl
	dec rcx
	cmp rcx, -1
	je .end
	jne .loop
.set_zero:
	mov rax, 0
	jmp .end
.end:
	pop rbx
	ret
