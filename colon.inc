%ifndef lastLabel
%define lastLabel 0

%macro colon 2
%2: 
  dq lastLabel
  db %1, 0
%define lastLabel %2
%endmacro

%endif
